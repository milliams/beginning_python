{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Conditionals\n",
    "\n",
    "We've seen loops are one way of changing the default \"top to bottom\" reading of Python scripts. Loops are an example of *control flow* statements. Another very useful tool in Python is the *conditional*. This, rather than allowing you to *repeat* parts of the program, gives you the ability to *skip* parts depending on certain conditions.\n",
    "\n",
    "The simplest place to start is the `if` statement. This lets you only run a block of code if a certain condition is true. Copy the following code into a file called `if.py` and run it with `python if.py` in the terminal:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting if.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile if.py\n",
    "\n",
    "my_number = 128\n",
    "\n",
    "if my_number > 100:\n",
    "    print(my_number, \"is large\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "128 is large\n"
     ]
    }
   ],
   "source": [
    "%run if.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To explore the behaviour of the `if` statement, we could edit the `if.py` script to change the value of `my_number`, but instead let's change it to that it's set via an `input` function call. The `input` function will always return a string, even if you enter digits so we need to explicitly convert the input into an integer with the [`int`](https://docs.python.org/3/library/functions.html#int) function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "tags": [
     "remove_cell"
    ]
   },
   "outputs": [],
   "source": [
    "%load_ext interactive_system_magic"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting if.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile if.py\n",
    "\n",
    "my_number = int(input(\"Enter a number: \"))  # We can nest function calls directly\n",
    "\n",
    "if my_number > 100:\n",
    "    print(my_number, \"is large\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Enter a number: 128\n",
       "128 is large"
      ]
     },
     "execution_count": 6,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/beginning_python/venv/bin/python3.10 if.py",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%%run_python_script -i if.py\n",
    "128"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Run the program with different inputs. Does it give you what you expect? What happens if the input is smaller than 100?\n",
    "\n",
    "[<small>answer</small>](answer_first_if.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `if` statement syntax\n",
    "\n",
    "An `if` statement has a similar sort of structure to a `for` loop in that it has scaffolding as well as user-supplied parts. The scafolding is the word `if` and the colon again:\n",
    "\n",
    "<pre>\n",
    " ↓                ↓\n",
    "<b style=\"color:darkred\">if</b> my_number > 100<b style=\"color:darkred\">:</b>\n",
    "    print(my_number, \"is large\")\n",
    "</pre>\n",
    "\n",
    "and the user-supplied part is the conditional:\n",
    "\n",
    "<pre>\n",
    "          ↓\n",
    "if <b style=\"color:darkred\">my_number > 100</b>:\n",
    "    print(my_number, \"is large\")\n",
    "</pre>\n",
    "\n",
    "As before, the body must be indented by four spaces:\n",
    "\n",
    "<pre>\n",
    "                <i>colon</i>\n",
    "                  ↓\n",
    "if my_number > 100<b style=\"color:darkred\">:</b>\n",
    "    print(my_number, \"is large\")\n",
    "  ↑\n",
    "<i>indentation</i>\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Booleans\n",
    "\n",
    "If we take a closer look at that user-supplied conditional we'll see it's made up of three parts, some data on either side of a greater-than sign (`>`). In Python this means \"is `my_number` more than `100`?\". It's asking a question and in Python the answer to a question like this can be either `True` or `False`.\n",
    "\n",
    "For example,\n",
    "\n",
    "```python\n",
    "128 > 100\n",
    "```\n",
    "\n",
    "is `True`, and:\n",
    "\n",
    "```python\n",
    "56 > 100\n",
    "```\n",
    "\n",
    "is `False`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results of these questions are *booleans*. `True` and `False` are values in the same way that `12` and `\"Hello\"` are values but belong to their own data type.\n",
    "\n",
    "Other boolean operations we can perform are:\n",
    "\n",
    "```python\n",
    "334 < 98  # Less than\n",
    "76 == 70 + 6  # Are they equal to each other?\n",
    "3.14159 != 3  # Are they *not* equal to each other\n",
    "4 <= 43  # Less than or equal to\n",
    "45 >= 17  # Greater than or equal to\n",
    "```\n",
    "\n",
    "Notice that when *comparing* two values, we use a double equals sign (`==`), wheras we used a single equals sign (`=`) to create a variable.\n",
    "\n",
    "We also used a `#` symbol in this code to denote a \"comment\". Comments are ignored by Python when running your code which means you can use them to explain to other humans reading your code what it's doing. This is a good idea if there's anything non-obvious in your code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Experiment with editing `if.py` to use some different boolean statements. Make sure you remember to save the file after each change before running it.\n",
    "\n",
    "[<small>answer</small>](answer_more_conditionals.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `else`\n",
    "\n",
    "We've just seen that the body of an `if` statement will only run if the conditional is `True`. But what if we want to do one thing if it's true, but another if it's false? We can do this by attaching an `else` statement to the `if` statement:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting if.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile if.py\n",
    "\n",
    "my_number = int(input(\"Enter a number: \"))\n",
    "\n",
    "if my_number > 100:\n",
    "    print(my_number, \"is large\")\n",
    "else:\n",
    "    print(my_number, \"is not large\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `else` statement must be at the same level of indentation as the `if` keyword and does not have any option for the user to provide a boolean statement to it. In this case, you can guarantee that *one* and *only one* of the two bodies will run.\n",
    "\n",
    "## `elif`\n",
    "\n",
    "If you *do* want to provide a boolean statement to an `else` then you can use an `elif` instead. It stands for \"else, if ...\" and it allows you to refine the questions you are asking:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting if.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile if.py\n",
    "\n",
    "my_number = int(input(\"Enter a number: \"))\n",
    "\n",
    "if my_number > 100:\n",
    "    print(my_number, \"is large\")\n",
    "elif my_number < 0:\n",
    "    print(my_number, \"is negative\")\n",
    "else:\n",
    "    print(my_number, \"is not large\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can again rely on at most one of the branches being run."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ordering your options\n",
    "\n",
    "When working out which lines of code will be run, Python will work down the list of `if`, `elif`s and `else` and will run the *first one* that matches. Once it's matched one, it will not bother checking to see if any of those later on would have matched. This means that you should order your questions from most-specific to least-specific.\n",
    "\n",
    "For example, if you want to do one thing for positive numbers, but something special instead for numbers greater than 100, then you should put the more specific check first:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting if.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile if.py\n",
    "\n",
    "my_number = int(input(\"Enter a number: \"))\n",
    "\n",
    "if my_number > 100:\n",
    "    print(my_number, \"is large\")\n",
    "elif my_number > 1:\n",
    "    print(my_number, \"is positive\")\n",
    "else:\n",
    "    print(my_number, \"negative\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Combining questions\n",
    "\n",
    "It is possible to ask two or more questions in one go by combining them with `and` and `or`. So, if you want to check is a number is smaller than ten (`my_number < 10`) and is not equal to zero (`my_number != 0`), you can use:\n",
    "\n",
    "```python\n",
    "if my_number < 10 and my_number != 0:\n",
    "    ...\n",
    "```\n",
    "\n",
    "These combined checks can be used is both `if` and `elif` statements."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Create a file `leap_year.py` which calculates, for a given year, whether it is a leap year and prints out the answer. You might want to read the year in using `input`, or you could hard-code it in the program.\n",
    "\n",
    "The rules to follow are:\n",
    "1. if the year is divisible by $400$ then it's a leap year,\n",
    "2. otherwise, if the year is divisible by $100$ then it's not,\n",
    "3. otherwise, if the year is divisible by $4$ then it's a leap year,\n",
    "4. otherwise, it's not.\n",
    "\n",
    "To simplify the writing of the program, you might find it easier to start with the divisible-by-$4$ condition, then add in the divisible-by-$100$ check and then add in the divisible-by-$400$ calculation.\n",
    "\n",
    "For reference, here are some years for you to check against:\n",
    "\n",
    "- $2023$ - not a leap year as it's not divisible by $4$\n",
    "- $2024$ - a leap year as it's divisible by $4$ (and not by $100$)\n",
    "- $1900$ - not a leap year as it's divisible by $100$ (and not by $400$)\n",
    "- $2000$ - a leap year as it's divisible by $400$\n",
    "\n",
    "[<small>answer</small>](answer_if_leap_year.ipynb)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
