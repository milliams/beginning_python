{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Loops\n",
    "\n",
    "In the introduction you were told that Python will read your script, starting at the top and running each line of code until it reaches the bottom. While largely true, it is possible to make Python repeat certain lines of code using *loops*.\n",
    "\n",
    "The ability to run a line of code multiple times is the first large step on your road to making your code *reusable*.\n",
    "\n",
    "Imagine we have two strings that we want to print. We could start by making a variable containing one of the words and then printing it:\n",
    "\n",
    "```python\n",
    "word = \"Hello\"\n",
    "\n",
    "print(word)\n",
    "```\n",
    "\n",
    "To print our second word, we could copy and paste those two lines to create a program which can print both words:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting loop.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile loop.py\n",
    "\n",
    "word = \"Hello\"\n",
    "\n",
    "print(word)\n",
    "\n",
    "word = \"Python\"\n",
    "\n",
    "print(word)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello\n",
      "Python\n"
     ]
    }
   ],
   "source": [
    "%run loop.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This printed the output we want. But we can see that this code is wasteful as the two `print` lines are identical to each other. They both print whatever the variable `word` is pointing at. If we can manage to write that line only once then we could save ourselves some typing!\n",
    "\n",
    "Let's start by making a container for our words. A Python `list` makes sense:\n",
    "\n",
    "```python\n",
    "my_words = [\"Hello\", \"Python\"]\n",
    "```\n",
    "\n",
    "we can now write a loop which will perform a task once for each word in our list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting loop.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile loop.py\n",
    "\n",
    "my_words = [\"Hello\", \"Python\"]\n",
    "\n",
    "for word in my_words:\n",
    "    print(word)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, when we run it, we should see that it prints the same output as our previous example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello\n",
      "Python\n"
     ]
    }
   ],
   "source": [
    "%run loop.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We've taken a script that was four lines of code and have reduced it to three lines. That might not seem like much of a reduction but the loop we wrote will work no matter how many items there are in the list `my_words`.\n",
    "\n",
    "Most loops in Python work by doing some set of actions for each item in the list. For this reason, this sort of loop is sometimes called a *for-each* loop.\n",
    "\n",
    "This maps to real life where you may want, for example, to buy each item on your shopping list. Another way of saying that could be \"for each item on my shopping list, buy the item\", or as you would write that in Python:\n",
    "\n",
    "```python\n",
    "for item in shopping_list:\n",
    "    buy(item)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Edit `loop.py` to have a different number of words in the list.\n",
    "- Does it work if you put numbers (i.e. integers or floats) in there as well?\n",
    "- What happens if the list `my_words` is empty?\n",
    "  - hint: empty lists look like `[]`\n",
    "\n",
    "[<small>answer</small>](answer_first_loop.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loop syntax breakdown\n",
    "\n",
    "Before we move on to other things we can do with loops, let's first make sure that we understand what's happening on those two lines of Python code that make up the loop.\n",
    "\n",
    "The first line is where most of the magic is hapenning and I like to break it down into five sections, three of fixed scaffolding and two where you as a programmer can have input.\n",
    "\n",
    "The scaffolding is the parts of the line which must always be the same and which Python uses to know that you're trying to make a loop. They're pointed out here as the word `for`, the word `in` and the colon (`:`) at the end of the line. These must always be there and in that order:\n",
    "\n",
    "<pre>\n",
    " ↓        ↓         ↓\n",
    "<b style=\"color:darkred\">for</b> word <b style=\"color:darkred\">in</b> my_words<b style=\"color:darkred\">:</b>\n",
    "    print(word)\n",
    "</pre>\n",
    "\n",
    "\n",
    "Once the scaffolding is in place, you can place between it the things that *you* care about. the first thing to think about is the object that you want to loop over. In our case we want to loop over the list `my_words` because we want to perform some action on every item in that list (we want to print the item):\n",
    "\n",
    "<pre>\n",
    "                ↓\n",
    "for word in <b style=\"color:darkred\">my_words</b>:\n",
    "    print(word)\n",
    "</pre>\n",
    "\n",
    "Now we have decided what object we are looping over, we need to decide what name we want to give temporarily to each item as we get to it. As with any variable naming, it is important that we choose a good name which describe a single object from the list. For example, if we're looping over all students in a class then we could call the variable `student` or if we're looping over a list of ages then we could call the variable `age`. The actual choice of variable name here does not affect how the code runs. We could use the name `sausage` and the code would run identically.\n",
    "\n",
    "Here, since we're looping over a list of generic *words*, we name our variable `word`:\n",
    "\n",
    "<pre>\n",
    "      ↓\n",
    "for <b style=\"color:darkred\">word</b> in my_words:\n",
    "    print(word)\n",
    "</pre>\n",
    "\n",
    "That's all that's required to tell Python that we're making a loop but if we want the loop to actually *do* something then we need to give the loop a *body*. The body is the lines of code that are going to be repeated. They can be any Python code but it is only within the body of the loop that we can refer to the loop variable `word`:\n",
    "\n",
    "<pre>\n",
    "for word in my_words:\n",
    "    <b style=\"color:darkred\">print(word)</b>       ← body of loop\n",
    "</pre>\n",
    "\n",
    "Finally, we get to a peculiarity of Python in that it uses indentation to decide what is in the body of the loop and what is not. Remember that it will only repeat the code in the body. All code in the body must be indented relative to the word `for` by four spaces. A trick to help remember this is that every time you see a colon in Python you should start a new line and indent:\n",
    "\n",
    "<pre>\n",
    "                  <i>colon</i>\n",
    "                    ↓\n",
    "for word in my_words<b style=\"color:darkred\">:</b>\n",
    "    print(word)\n",
    "  ↑\n",
    "<i>indentation</i>\n",
    "</pre>\n",
    "\n",
    "If we want to write code after the end of a loop, we have to make sure that it is *not* indented. So this code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting loop.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile loop.py\n",
    "\n",
    "my_words = [\"Hello\", \"Python\"]\n",
    "\n",
    "for word in my_words:\n",
    "    print(word)\n",
    "\n",
    "print(\"...Goodbye\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "will print:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello\n",
      "Python\n",
      "...Goodbye\n"
     ]
    }
   ],
   "source": [
    "%run loop.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "but this code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting loop.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile loop.py\n",
    "\n",
    "my_words = [\"Hello\", \"Python\"]\n",
    "\n",
    "for word in my_words:\n",
    "    print(word)\n",
    "\n",
    "    print(\"...Goodbye\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "will print"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello\n",
      "...Goodbye\n",
      "Python\n",
      "...Goodbye\n"
     ]
    }
   ],
   "source": [
    "%run loop.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See how the `...Goodbye` was repeated in the second example, this is because it was inside the body of the loop since it was indented."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Extended: What can we loop over\n",
    "\n",
    "A lot of the power of loops comes from being able to put a lot of different things in the place of `my_words`.\n",
    "\n",
    "Most simply, instead of putting a variable name there, you can put a list directly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting loop.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile loop.py\n",
    "\n",
    "for word in [\"Hello\", \"Python\"]:\n",
    "    print(word)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As well as lists we can put anything which Python considers *iterable*. For now we haven't come across many of those but as we keep learning we'll discover many more. One that we have already come across is strings:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting loop.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile loop.py\n",
    "\n",
    "phrase = \"Hello Python\"\n",
    "\n",
    "for letter in phrase:\n",
    "    print(letter)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Looping over a string will always give you one letter at a time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Experiment with `loop.py` and make it loop over both lists and strings.\n",
    "\n",
    "[<small>answer</small>](answer_loop_list_string.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Extended: Ranges of numbers\n",
    "\n",
    "There's a built in function in Python called `range` which provides you with numbers in a range. If given one number as an argument it will give you integers, starting from zero and going up to, *but not including*, the number you gave as an agument. We can put this call to the `range` function directly into our loop as the object to loop over:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting range.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile range.py\n",
    "\n",
    "for number in range(5):\n",
    "    print(number)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "will print:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0\n",
      "1\n",
      "2\n",
      "3\n",
      "4\n"
     ]
    }
   ],
   "source": [
    "%run range.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `range` function can also be given two arguments, in which case, the first argument is the number to start counting from and the second argument is used as above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting range.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile range.py\n",
    "\n",
    "for number in range(10, 13):\n",
    "    print(number)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "printing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "10\n",
      "11\n",
      "12\n"
     ]
    }
   ],
   "source": [
    "%run range.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Extended: Enumerating\n",
    "\n",
    "When looping over a list we are provided with one element at a time which we give a name (`word` in our example earlier) to be used inside the loop body. However, we don't have any context about, for example, what the index of the item is from the list.\n",
    "\n",
    "To be able to loop over a list and keep track of both the index of the item and the item itself we can use the `enumerate` function. This function, if given a list, can give to the loop both pieces of information at once.\n",
    "\n",
    "<pre>\n",
    "                          ↓\n",
    "for index, word in <b style=\"color:darkred\">enumerate(my_words)</b>:\n",
    "    print(word, \"is at index\", index)\n",
    "</pre>\n",
    "\n",
    "Since each time through the loop we are being given not just one piece of information (the element) but two (the index *and* the element) we need to provide two loop variable names. We do this by giving both names separated by commas in the usual place:\n",
    "\n",
    "<pre>\n",
    "      ↓     ↓\n",
    "for <b style=\"color:darkred\">index, word</b> in enumerate(my_words):\n",
    "    print(word, \"is at index\", index)\n",
    "</pre>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Write a script (`loop_exercise.py`) which creates a list of floats and loops over them, printing the index of the element as well as the value of the float.\n",
    "\n",
    "[<small>answer</small>](answer_loop_floats_enumerate.ipynb)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
