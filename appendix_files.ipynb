{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Files\n",
    "\n",
    "Everything we've done so far has been completely self-contained in the script and every time we run any of them we will get exactly the same output. The power of programming is to be able to take the same piece of code and apply it to different data to get different results. One common way in which this is done is writing a script which can analyse a data file. To do that we need to learn how to open files.\n",
    "\n",
    "The simplest this we can do with files is read a file in and print it to the screen. Make a new script called `file.py` and put the following in it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting file.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile file.py\n",
    "\n",
    "with open(\"file.py\") as f:\n",
    "    for line in f:\n",
    "        print(line, end=\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you run it, you will see the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "with open(\"file.py\") as f:\n",
      "    for line in f:\n",
      "        print(line, end=\"\")\n"
     ]
    }
   ],
   "source": [
    "%run file.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "which is (somewhat recursively) the contents of the file `file.py`.\n",
    "\n",
    "There are a few new things here so let's go through them in turn. The first thing is to open the file. You open files using the `open` function. The part `open(\"file.py\")` says to open the file `file.py`. This returns a *file handle* which is assigned to the variable `f`. If the file does not exist, or is not readable then the script will exit with an error (have a try and see what the error looks like!). The use of a `with` statement means that when the code inside the `with` block has finished running the file will be closed automatically.\n",
    "\n",
    "In the next line (`for line in f:`) we are looping over the lines of the file. This loop looks just like those we used when looping over lists a few chapters previously. When looping over a list you get each of the *elements* in turn but when looping over an open file you get each of the *lines* in turn. We assign the string containing the line from the file to the variable `line`.\n",
    "\n",
    "Finally, we print the string `line`. Each line in the file already ends with a \"new-line\" character so when it is printed, it will print the new-line too. By default the `print` function will also add its own new-line so we disable that by using `end=\"\"`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "- Printing out Python code isn't the most useful so let's make a data file to read instead. Make a new file called `data.txt` and put inside it:\n",
    "\n",
    "  ```\n",
    "  12\n",
    "  54\n",
    "  7\n",
    "  332\n",
    "  54\n",
    "  1\n",
    "  0\n",
    "  ```\n",
    "- Edit `file.py` so that it prints out the contents of `data.txt` instead. [<small>answer</small>](answer_first_read_data.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data type conversion\n",
    "\n",
    "Simply reading the data and printing it isn't very useful. Let's take a first step towards some data analysis and pretend that the task we're trying to do is to read in data from the file and add 17 to each value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting file.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile file.py\n",
    "\n",
    "with open(\"data.txt\") as f:\n",
    "    for line in f:\n",
    "        new_number = line + 17  # Here is where we do our \"data analysis\"\n",
    "        print(new_number, end=\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you edit `file.py` to contain this code and run it you should see an error:\n",
    "\n",
    "```\n",
    "Traceback (most recent call last):\n",
    "  File \"file.py\", line 3, in <module>\n",
    "    new_number = line + 17\n",
    "TypeError: can only concatenate str (not \"int\") to str\n",
    "```\n",
    "\n",
    "This is telling us that there is an error occuring when trying to add 17 to the data read in from the line in the file. The *type* of the error is `TypeError` which tells us the problem is likely due to incorrect data types (i.e. string, float, int, list etc.). The error message says `can only concatenate str (not \"int\") to str` which implies that the computer believes that we're trying to concatenate (join together) something with a string. The only two things involved in this operation are `line` and `17`. We know that `17` is an integer so `line` must be a string!\n",
    "\n",
    "When reading from a file like this, everything it gives you will *always* be a string, even if the string only contains digits like `\"12\"`. If we know that the file only contains integers then we can convert each number as it comes in using [the `int` function](https://docs.python.org/3/library/functions.html#int). Also, since we're now printing integers, we no longer need the `end=\"\"` tweak:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "tags": [
     "remove_cell"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting data.txt\n"
     ]
    }
   ],
   "source": [
    "%%writefile data.txt\n",
    "12\n",
    "54\n",
    "7\n",
    "332\n",
    "54\n",
    "1\n",
    "0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting file.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile file.py\n",
    "\n",
    "with open(\"data.txt\") as f:\n",
    "    for line in f:\n",
    "        number = int(line)  # Here we do the type conversion\n",
    "        new_number = number + 17  # Here is where we do our \"data analysis\"\n",
    "        print(new_number)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running this new script will now print out our \"processed\" data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "29\n",
      "71\n",
      "24\n",
      "349\n",
      "71\n",
      "18\n",
      "17\n"
     ]
    }
   ],
   "source": [
    "%run file.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "- Change `file.py` to multiply the data by 10 instead of adding 17. [<small>answer</small>](answer_multiply_data.ipynb)\n",
    "- After looping though the data, print out the sum of all the data values seen.\n",
    "  - hint: Make an integer before the loop, initially set to zero and add to it each time around the loop\n",
    "  - hint: You can increase an integer by an amount using `+=` like:\n",
    "    ```\n",
    "    num = 3\n",
    "    num += 4\n",
    "    print(num)  # `num` will now be 7\n",
    "    ```\n",
    "  - [<small>answer</small>](answer_data_sum.ipynb)\n",
    "- Print out the count of the number of data points seen as well. [<small>answer</small>](answer_data_sum_count.ipynb)\n",
    "- Print out the mean average of the data in the file. [<small>answer</small>](answer_data_mean.ipynb)\n",
    "- See what happens if you run the script after deleting the contents of `data.txt`. Add an `if` statement to fix it. [<small>answer</small>](answer_divide_zero.ipynb)\n",
    "- Collect the statistics into a summary dictionary with keys `\"sum\"`, `\"count\"` and `\"mean\"`. [<small>answer</small>](answer_data_summary_dict.ipynb)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
