{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Data types\n",
    "\n",
    "## Strings\n",
    "\n",
    "In the last chapter we introduced the idea of \"strings\". They are a way of representing normal human words inside a Python script. Strings start and end with double quotes (`\"`) e.g.,\n",
    "\n",
    "```python\n",
    "\"Hello from Python!\"\n",
    "```\n",
    "\n",
    "is a string with three words and an exclamation mark as content.\n",
    "\n",
    "Strings can contain numbers as well:\n",
    "\n",
    "```python\n",
    "\"The Battle of Hastings was in 1066\"\n",
    "```\n",
    "\n",
    "and they can even be empty:\n",
    "\n",
    "```python\n",
    "\"\"\n",
    "```\n",
    "\n",
    "It is possible in Python to also use single quotes (`'`) to make strings, as long as the string starts and ends with the same type of quote, but it is convention to prefer double quotes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Edit the file `script.py` to print out your name so that when it's run it shows something like:\n",
    "\n",
    "```\n",
    "Hello Matt\n",
    "```\n",
    "\n",
    "[<small>answer</small>](answer_print_different_string.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Numbers\n",
    "\n",
    "The other most common type of data that you'll find in Python scripts are numbers. There are two main types of number in Python:\n",
    "\n",
    "- **Floats** (or *floating point* numbers) are a way of representing numbers with decimal places\n",
    "- **Integers** are for representing whole numbers\n",
    "\n",
    "When creating numbers in Python, you do not use quotes, you write the number directly. So:\n",
    "\n",
    "```python\n",
    "3.14159\n",
    "```\n",
    "\n",
    "is a float and\n",
    "\n",
    "```python\n",
    "42\n",
    "```\n",
    "\n",
    "is an integer.\n",
    "\n",
    "It's important that when writing numbers in your scripts, you do not put quotation marks around them. There is a difference between `42` and `\"42\"`. The first is the integer $42$ (i.e. the number one greater that $41$) and the second is just a pair of digits and has no more meaning to Python than `\"fourty two\"` or `\"penguin\"` do."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Variables\n",
    "\n",
    "Of course, it not much use to have numbers and strings floating about with no connection to each other. We want to be able to give them names and combine them together. We assign names to data using the `=` sign. For example if we want to make some data and give it a name we can do it like:\n",
    "\n",
    "```python\n",
    "pi = 3.14159\n",
    "```\n",
    "\n",
    "This has created a number `3.14159` and given it a name, `pi`. We can now use this name in other parts of the program to refer to that piece of data:\n",
    "\n",
    "```python\n",
    "print(pi)\n",
    "```\n",
    "\n",
    "Names in Python can contain upper and lower case letters, numbers and underscores (but can't start with a number). Chosing the correct name for a particular variable is an important task as a non-descriptive name (or worse, an incorrect name) will be very confusing for you and anyone reading your code. It is common in Python to name your variables with all lower case letters and use underscores to separate words.\n",
    "\n",
    "So, for a variable which contains a number representing a distance in miles, avoid shortened names like `dm`, `distm` or `d` and instead use a name like `distance_in_miles`. Remember, code will be written once but read many times so make it easy to read."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These two can be combined into a full Python script:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Writing variables.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile variables.py\n",
    "\n",
    "pi = 3.14159\n",
    "print(pi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and run with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3.14159\n"
     ]
    }
   ],
   "source": [
    "%run variables.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Edit your `script.py` to split it over two lines. The first line should create a string and give it a variable name and the second line should use that name to print. Make sure you save the file and rerun it in the terminal.\n",
    "\n",
    "[<small>answer</small>](answer_two_line_print.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Combining variables\n",
    "\n",
    "Variables are more that just a way of labelling data, they also make it easier to *do* things with your data. If you have some numbers you can add, subtract, multiply and divide them as you would expect. The symbol for multiplication is `*` and the symbol for division is `/`.\n",
    "\n",
    "```python\n",
    "distance_in_miles = 30\n",
    "distance_in_km = distance_in_miles * 1.60934\n",
    "\n",
    "print(distance_in_km)\n",
    "```\n",
    "\n",
    "Here we created a variable `distance_in_miles` with the value of `30`. Then we used that variable in line two and multiplied it by a number (`distance_in_miles * 1.60934`) and assigned the result of that calculation to a new variable called `distance_in_km`. Finally, we printed out the new variable.\n",
    "\n",
    "Likewise we can do addition:\n",
    "\n",
    "```python\n",
    "temperature_in_celcius = 25.1\n",
    "temperature_in_kelvin = temperature_in_celcius + 273.15\n",
    "\n",
    "print(temperature_in_kelvin)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Printing multiple things\n",
    "\n",
    "So far we've been giving the `print` function a single argument to print a single thing but we can print many things at once if we give it multiple arguments. Arguments to functions in Python are separated by commas. The `print` function is designed so that it will print each of the arguments it was provided with, one after another on the same line, separated by spaces."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting colour.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile colour.py\n",
    "\n",
    "fav = \"red\"\n",
    "\n",
    "print(\"My favourite colour is\", fav)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "My favourite colour is red\n"
     ]
    }
   ],
   "source": [
    "%run colour.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Edit `script.py` so that the two parts of the phrase are passed to `print` as separate arguments. Make sure you save the file and rerun it in the terminal.\n",
    "\n",
    "If you get strange output like `('Hello', 'Python')` rather than `Hello Python`, then try running your script with the `python3` command as described in [this video](https://www.youtube.com/watch?v=Fw6VYgsbJ4o).\n",
    "\n",
    "[<small>answer</small>](answer_multiple_print.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Getting data into your script\n",
    "\n",
    "So far, all the code we've run is somewhat static. Every time we run the script, the output will always be the same. What if your favourite colour is not red? The power of programming is that it is dynamic and we can write code which responds and reacts. We'll see a lot more of this throughout this workshop, but for now we'll introduce one more function that Python provides, [`input`](https://docs.python.org/3/library/functions.html#input).\n",
    "\n",
    "The `print` function is how we get information out of our program, and the `input` function is how we get data *into* it.\n",
    "\n",
    "The `input` function will pause the program and wait for you to type something in. Whatever you type, followed by enter will be assigned to whatever variable name you put on the left hand side of the `=`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "tags": [
     "remove_cell"
    ]
   },
   "outputs": [],
   "source": [
    "%load_ext interactive_system_magic"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting colour.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile colour.py\n",
    "\n",
    "print(\"What is your favourite colour?\")\n",
    "\n",
    "fav = input()\n",
    "\n",
    "print(\"My favourite colour is\", fav)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, if we run this script, it will print the first message, and then wait for you to type something. If you type \"red\" and then press enter, it will assign \"red\" to the variable `fav` and then use that variable in the final `print` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "What is your favourite colour?\n",
       "red\n",
       "My favourite colour is red"
      ]
     },
     "execution_count": 17,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/beginning_python/venv/bin/python3.10 colour.py",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%%run_python_script -i colour.py\n",
    "red"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Likewise, you can run the script one more time and give a different answer and get a different output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "What is your favourite colour?\n",
       "blue\n",
       "My favourite colour is blue"
      ]
     },
     "execution_count": 18,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/beginning_python/venv/bin/python3.10 colour.py",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%%run_python_script -i colour.py\n",
    "blue"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The prompt\n",
    "\n",
    "One small improvement we can make is that instead of printing our message \"What is your favourite colour?\", prompting the user of the script to type something, we can do this directly with the `input` function by giving the prompt as an argument:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting colour.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile colour.py\n",
    "\n",
    "fav = input(\"What is your favourite colour? \")\n",
    "\n",
    "print(\"My favourite colour is\", fav)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that there is a space after the question mark so that when you're typing in your answer it's not butting directly against the character:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "What is your favourite colour? red\n",
       "My favourite colour is red"
      ]
     },
     "execution_count": 14,
     "metadata": {
      "text/x.prog": {
       "command": "/home/matt/projects/courses/beginning_python/venv/bin/python3.10 colour.py",
       "returncode": 0
      }
     },
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%%run_python_script -i colour.py\n",
    "red"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Edit `script.py` so the value of the variable `name` is set using the `input` function. Be sure to set a prompt as well so that the user know that they should type something.\n",
    "\n",
    "The script should, when run with `python script.py`, then print out:\n",
    "\n",
    "```\n",
    "What is your name?\n",
    "```\n",
    "\n",
    "and wait for you to type your name like:\n",
    "\n",
    "```\n",
    "What is your name? Matt\n",
    "```\n",
    "\n",
    "After pressing enter, it should then print out:\n",
    "\n",
    "```\n",
    "What is your name? Matt\n",
    "Hello Matt\n",
    "```\n",
    "\n",
    "[<small>answer</small>](answer_hello_input.ipynb)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
