{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dictionaries\n",
    "\n",
    "Lists let you store lots of variables, and to access them by their location in the list. However, there are times when you want to store lots of variables, but access them using more complex relationships. One example is a *dictionary*, which lets you store variables and access them using a *key*.\n",
    "\n",
    "Dictionaries in Python are created using curly brackets. Make a new file called `dict.py` and put this in it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting dict.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile dict.py\n",
    "\n",
    "sounds = {\"cat\": \"meow\", \"dog\": \"woof\", \"horse\": \"neigh\"}\n",
    "\n",
    "cat_sound = sounds[\"cat\"]\n",
    "\n",
    "print(cat_sound)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "meow\n"
     ]
    }
   ],
   "source": [
    "%run dict.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What we did here was create a dictionary on the first line. A dictionary is created using curly brackets (`{}`), in much the same way as square brackets are used for creating lists. The dictionary we created here has three items in it where each item comprises a *key* and a *value*. The *value* is the real data that we want to keep hold of and the *key* is how we can get at the data we want. The key and value are separated by a colon and each key-value pair is separated by a comma.\n",
    "\n",
    "On the next line we access the data in the dictionary `sounds`. Again, like lists we use the square brackets to ask questions of our data. In this case we're asking the dictionary to give us the value associated with the key `\"cat\"` and so it will return to us `\"meow\"`.\n",
    "\n",
    "Since dictionaries can be quite large and it can sometimes be hard to see which parts are keys and which are values, it is possible to write dictionaries over multiple lines, one line per key-value item:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting dict.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile dict.py\n",
    "\n",
    "sounds = {\n",
    "    \"cat\": \"meow\",\n",
    "    \"dog\": \"woof\",\n",
    "    \"horse\": \"neigh\"\n",
    "}\n",
    "\n",
    "cat_sound = sounds[\"cat\"]\n",
    "\n",
    "print(cat_sound)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Edit `dict.py` to ask for the sound for the dog and the horse.\n",
    "- What happens if you ask for an animal that isn't in the dictionary?\n",
    "\n",
    "[<small>answer</small>](answer_first_dictionary.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Adding new data into dictionaries\n",
    "\n",
    "As with lists, dictionaries are dynamic so we can add entries into a dictionary.\n",
    "\n",
    "Let's say that we want to add in a new sound for a cow into our `sounds` dictionary. The key that the data will have will be `\"cow\"` and the value will be `\"moo\"`. To do so we put `sounds[\"cow\"]` on the left-hand side of a variable assignment expression, as if we're making a new variable. On the right goes the data that we want to put into the dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting dict.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile dict.py\n",
    "\n",
    "sounds = {\n",
    "    \"cat\": \"meow\",\n",
    "    \"dog\": \"woof\",\n",
    "    \"horse\": \"neigh\"\n",
    "}\n",
    "\n",
    "sounds[\"cow\"] = \"moo\"\n",
    "\n",
    "print(sounds)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is saying that we want the value `\"moo\"` associated with the key `\"cow\"` in the dictionary `sounds`.\n",
    "\n",
    "Running it, we see:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'cat': 'meow', 'dog': 'woof', 'horse': 'neigh', 'cow': 'moo'}\n"
     ]
    }
   ],
   "source": [
    "%run dict.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Edit `dict.py` so that the dictionary is initially defined with only the cat and dog entries. Add the entry for the horse and then the cow dynamically.\n",
    "\n",
    "[<small>answer</small>](answer_add_to_dictionary.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Looping over dictionaries\n",
    "\n",
    "When discussing `for` loops you were told that Python allows you to loop over lots of different types of data such as lists, strings and `range`s. We can add dictionaries to that set.\n",
    "\n",
    "To discover how it works, let's do the naïve thing first and just see what happens when we loop over a dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting dict_loop.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile dict_loop.py\n",
    "\n",
    "sounds = {\n",
    "    \"cat\": \"meow\",\n",
    "    \"dog\": \"woof\",\n",
    "    \"horse\": \"neigh\"\n",
    "}\n",
    "\n",
    "for thing in sounds:\n",
    "    print(thing)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "cat\n",
      "dog\n",
      "horse\n"
     ]
    }
   ],
   "source": [
    "%run dict_loop.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hopefully, you recognise those as the keys from the dictionary. So, it seems that when looping over a dictionary we will be given the *keys*.\n",
    "\n",
    "What if, for example, you wanted to loop over the *values* instead. Well, there is a method on dictionaries called `values` which gives you just those so that you can loop over them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting dict_loop.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile dict_loop.py\n",
    "\n",
    "sounds = {\n",
    "    \"cat\": \"meow\",\n",
    "    \"dog\": \"woof\",\n",
    "    \"horse\": \"neigh\"\n",
    "}\n",
    "\n",
    "for sound in sounds.values():\n",
    "    print(sound)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "meow\n",
      "woof\n",
      "neigh\n"
     ]
    }
   ],
   "source": [
    "%run dict_loop.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we want to loop over the dictionary and get both the keys *and* the values, there is a method called `items`. Since it will be giving us two things each loop iteration, we'll have to use the same trick as we did with `enumerate` and give two variable names in the `for` loop declaration:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting dict_loop.py\n"
     ]
    }
   ],
   "source": [
    "%%writefile dict_loop.py\n",
    "\n",
    "sounds = {\n",
    "    \"cat\": \"meow\",\n",
    "    \"dog\": \"woof\",\n",
    "    \"horse\": \"neigh\"\n",
    "}\n",
    "\n",
    "for animal, sound in sounds.items():\n",
    "    print(animal, \"goes\", sound)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "cat goes meow\n",
      "dog goes woof\n",
      "horse goes neigh\n"
     ]
    }
   ],
   "source": [
    "%run dict_loop.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `items` method gives us two pieces of data where the first is always the key and the second if always the value. We give the keys the name `animal` and the values the name `sound`. We can then use both those variables in the loop body."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Uses for dictionaries\n",
    "\n",
    "Dictionaries can be used for any key-value mapping. The example above was a mapping of an animal species (as a string) to an animal sound ( as a string). You can use any data type you wish as the value in a dictionary. For example you might make a dictionary containing the population of some cities in millions (as a float):\n",
    "\n",
    "```python\n",
    "census = {\n",
    "    \"London\": 8.615,\n",
    "    \"Paris\": 2.244,\n",
    "    \"Rome\": 2.627,\n",
    "}\n",
    "```\n",
    "\n",
    "or one which contains a list of authors as the key (as a string) and their books (as a list of strings):\n",
    "\n",
    "```python\n",
    "bookshelf = {\n",
    "    \"Terry Pratchett\": [\"Mort\", \"Jingo\", \"Truckers\"],\n",
    "    \"Jane Austen\": [\"Sense and Sensibility\", \"Pride and Prejudice\"],\n",
    "    \"Charles Dickens\": [\"Oliver Twist\"],\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise #\n",
    "\n",
    "Make a dictionary with the keys being the name of countries and the value being the country's capital city. Loop over the dictionary and print something like `\"The capital of France is Paris\"` for each item.\n",
    "\n",
    "[<small>answer</small>](answer_country_dictionary.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To summarise the different things we can pass to loops and the data that we get given each iteration:\n",
    "\n",
    "- `list`: the items in the list\n",
    "- `str`: the characters in the string\n",
    "- `enumerate()`: a pair of the index of the item and the item itself\n",
    "- `dict`: the *keys* of the dictionary\n",
    "- `dict.keys()`: the keys from the dictionary\n",
    "- `dict.values()`: the values from the dictionary\n",
    "- `dict.items()`: the key-value pairs from the dictionary"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
